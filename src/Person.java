import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;

@Component
public class Person {
    private int id;
    private String name;
    private String designation;
    private List<String> contactNumbers;
    private List<String> projects;

    public Person(@Value("1") int id, @Value("John Doe") String name, 
                  @Value("Manager") String designation, 
                  @Value("${person.contactNumbers}") List<String> contactNumbers, 
                  @Value("${person.projects}") List<String> projects) {
        this.id = id;
        this.name = name;
        this.designation = designation;
        this.contactNumbers = contactNumbers;
        this.projects = projects;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesignation() {
        return designation;
    }

    public List<String> getContactNumbers() {
        return contactNumbers;
    }

    public List<String> getProjects() {
        return projects;
    }
}
