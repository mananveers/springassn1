# SpringAssn1

Contains source code to satisfy the following requirments:

```
Create a class Person with following fields:
--- id
--- name
--- designation
--- contact numbers (List of Strings)
--- projects (List of Strings)

The program should demonstrate the following:
1. Create object using component annotation
2. Add values using value annotation
```

The directory is not set up to be run as-is.


Classes need to be added to a working workplace project with dependencies to be executed.